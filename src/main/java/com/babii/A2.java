package com.babii;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class A2 {

    private static Logger logger = LogManager.getLogger(A2.class);

    public A2() {
        logger.trace("This is trace message; the file will be overwritten every day");
    }
}
