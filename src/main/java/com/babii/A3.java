package com.babii;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class A3 {
    private static Logger logger = LogManager.getLogger(A3.class);

    public A3() {
        logger.warn("This is warning message; the file will be overwritten after reaching the size of 1 MB");
    }
}
