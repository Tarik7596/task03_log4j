package com.babii;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class A7 {
    private static Logger logger = LogManager.getLogger(A7.class);

    public A7() {
        logger.error("This is error message; it will be sent on e-mail");
    }
}
