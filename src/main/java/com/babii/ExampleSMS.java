package com.babii;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC5009ef2c78aac589bf805c62660cb44c";
    public static final String AUTH_TOKEN = "b0362772159601779da10436414637f5";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380671622242"), /*my phone number*/
                        new PhoneNumber("+16198701956"), str) .create(); /*attached to me number*/
    }
}
